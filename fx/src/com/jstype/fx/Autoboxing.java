/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.fx;



public class Autoboxing {
	
    @EnsureIdentifier
    public Byte bb(byte b) {
        return new Byte(b);
    }
    @EnsureIdentifier
    public Character bc(char c) {
        return new Character(c);
    }
    @EnsureIdentifier
    public Short bs(short s) {
        return new Short(s);
    }
    @EnsureIdentifier
    public Integer bi(int i) {
        return new Integer(i);
    }
    @EnsureIdentifier
    public Long bl(long l) {
        return new Long(l);
    }
    @EnsureIdentifier
    public Float bf(float f) {
        return new Float(f);
    }
    @EnsureIdentifier
    public Double bd(float d) {
        return new Double(d);
    }
    @EnsureIdentifier
    public Boolean bbl(boolean b) {
        return b ? Boolean.TRUE : Boolean.FALSE;
    }
}
