/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.fx;

/**
 * http://en.wikibooks.org/wiki/Java_Programming/Types
 * http://java.sun.com/docs/books/jls/second_edition/html/conversions.doc.html
 */
final public class TypeCast {

	/**
	 * This function checks the instance object
	 * @param t object to check for type
	 * @param c Constructor to check the instance 
	 * @return true if t is instance of c
	 */
	static native boolean instanceOf(Object t, Object c)
	/*-{
	return (t==null || t==undefined) ?
		false
		:	
		(t._i && c.prototype && c.prototype._id) ?
			c.prototype._id in t._i
			:
			t instanceof c;
	}-*/;

	/**
	 * Check if t is a instance of c , throw an Exception if not else return t
	 * @param t
	 * @param c
	 * @return
	 * @throws ClassCastException
	 */
	static Object castTo(Object t, Object c) throws ClassCastException {
		if (t == null || instanceOf(t, c)) {
			return t;
		}
		throw new ClassCastException("Can not cast to " + c.toString());
	}

	/**
	 * Convert double integer to byte
	 * @param v
	 * @return
	 */
	@EnsureIdentifier
	static native byte nb(double v)
	/*-{
		return v << 24 >> 24;
	}-*/;
	
	/**
	 * Narrow down to character
	 * @param v
	 * @return
	 */
	@EnsureIdentifier
	static native char nc(double v)
	/*-{
		return v & 0xFFFF;
	}-*/;
	
	/**
	 * Narrow down to intger
	 * @param v
	 * @return
	 */
	@EnsureIdentifier
	static native int ni(double v)
	/*-{
		return ~~v;
	}-*/;

	/**
	 * Narrow down to short
	 * @param v
	 * @return
	 */
	@EnsureIdentifier
	static native short ns(double v)
	/*-{
		return v << 16 >> 16;
	}-*/;

	/**
	 * Round to byte
	 * @param x
	 * @return
	 */
	@EnsureIdentifier
	static byte rb(double x) {
		return nb(ri(x));
	}

	/**
	 * Round to character
	 * @param x
	 * @return
	 */
	@EnsureIdentifier
	static char rc(double x) {
		return nc(ri(x));
	}

	/**
	 * Round to integer
	 * @param x
	 * @return
	 */
	@EnsureIdentifier
	static native int ri(double x)
	/*-{
		return ~~Math.max(Math.min(x, 2147483647), -2147483648);
	}-*/;

	/**
	 * Round to short
	 * @param x
	 * @return
	 */
	@EnsureIdentifier
	static short rs(double x) {
		return ns(ri(x));
	}

	/**
	 * Check if argument is null
	 * @param o
	 * @return
	 */
	@EnsureIdentifier
	static native boolean isNull(Object o)
	/*-{
		return o==null;
	}-*/;
}
