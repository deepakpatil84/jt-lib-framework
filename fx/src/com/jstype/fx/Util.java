/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.fx;

public class Util {
	/**
	 * Returns the cloned array
	 * Note:Only array is cloned not the values
	 * 
	 * @param array
	 *            Array to clone
	 * @return cloned array, null if source array is null
	 */
	public static <T> T[] clone(T[] array) {
		return Util.<T> cloneRange(array, 0, array.length);
	}

	/**
	 * Clones specific range of array
	 * 
	 * @param array
	 *            Array to clone
	 * @param start
	 *            start index
	 * @param end
	 *            end index
	 * @return cloned array , returns null if source is null
	 */
	public native static <T> T[] cloneRange(T[] array, int start, int end)
	/*-{
	    if(array==null) return null;
	    var i,a=[];
	    for(i=start;i<end;i++){
	       a.push(array[i]);
	    }
	    return a;
	}-*/;

	/**
	 * Create array of specified size, all values are initialized to null
	 * 
	 * @param size
	 *            size of the array
	 * @return Array of specified size
	 */
	public native <T> T[] createArray(int size)
	/*-{
	    var i=0,a=[];
	    for(i=0;i<size;i++){
	        a.push(null);
	    }
	    return a;
	}-*/;
	
	/**
	 * returns the Class instance for specified class name
	 * @param name
	 * @return
	 */
	public static native Class getClassObject(String name)
	/*-{        
    	var o=@java.lang.Class::new(Ljava/lang/String)(name);
    	o.__CLS[name]=o;
    	return o;
	}-*/;

}
